import networkx as nx
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import base64
from io import BytesIO

def start(state):
    A = pd.read_csv("/home/ali/Projects/project/startDjango/myStatistics/sources/asli.csv")
    B = pd.read_csv("/home/ali/Projects/project/startDjango/myStatistics/sources/asli2.csv")
    C = pd.read_csv("/home/ali/Projects/project/startDjango/myStatistics/sources/asli3.csv")

    list = {
        'A': A,
        'B': B,
        'C': C,
        "ALL": A + B + C,
    }

    df = list[state]

    G = nx.Graph()

    G = nx.from_pandas_edgelist(df, 'from', 'to')

    # G.add_edge('kalantari','dafater',relation ='friend')
    # G.add_edge('kalantari','dadsetan' ,relation ='friend')
    # G.add_edge('kalantari','dafater',relation ='neighbor')
    # G.add_edge('kalantari','dadsetan',relation ='neighbor')

    G.add_edge('dafater', 'ghozat', weight=20)
    G.add_edge('kalantari', 'ghozat', weight=20)
    G.add_edge('dadsetan', 'ghozat', weight=20)

    G.add_edge('ghozat', 'jormnist', weight=20)
    G.add_edge('jormnist', 'makhtome', weight=70)

    G.add_edge('ghozat', 'jormhast', weight=100)
    G.add_edge('jormhast', 'pishnevis', weight=70)
    G.add_edge('pishnevis', 'tozih_shakhs', weight=100)
    G.add_edge('tozih_shakhs', 'madarek', weight=70)
    G.add_edge('madarek', 'pezeshki_y', weight=30)
    G.add_edge('pezeshki_y', 'agahi_y', weight=15)
    G.add_edge('agahi_y', 'mahal', weight=40)
    G.add_edge('mahal', 'ghozat', weight=30)

    plt.figure(figsize=(10, 10))
    nx.draw_networkx(G)
    nx.spring_layout(G)
    return get_graph()


def get_graph():
    buffer = BytesIO()
    plt.savefig(buffer, format='png')
    buffer.seek(0)
    img_png = buffer.getvalue()
    graph = base64.b64encode(img_png)
    graph = graph.decode('utf-8')
    buffer.close()
    return graph
