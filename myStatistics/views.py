from django.shortcuts import render
from django.http import HttpResponse
from . import code


# Create your views here.
def home(request):
    return render(request, 'home.html')


def first(request):
    return render(request, 'show.html', {'plot': code.start('A')})


def second(request):
    return render(request, 'show.html', {'plot': code.start('B')})


def third(request):
    return render(request, 'show.html', {'plot': code.start('C')})


def all(request):
    return render(request, 'show.html', {'plot': code.start('ALL')})
